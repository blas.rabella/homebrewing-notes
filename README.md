# Homebrewing beer utilities

## The snippet

The directory `snippet` contains a file that is a [`yasnippet`](https://github.com/joaotavora/yasnippet) template for some calculations using `org-mode` and a barebones first day brewing log.
It relies on the `elisp` file for the computations as it simplifies the formulas in the tables.

Just copy it to something like `~/.emacs.d/snippets/org-mode/hbc` and adding `hbc` will be expanded to this sheet.

## The elisp file

The directory `elisp` contains a package `homebrewing` that defines functions to compute important numbers for your brewing day.

To install it, how I did is:

Create a directory such as `~/.emacs.d/elisp/` and copy the `homebrewing.el` there.

Then in your `.emacs` or `init.el` or whatever add the lines

```elisp
(add-to-list 'load-path (expand-file-name "elisp" user-emacs-directory))

(require 'homebrewing)
```

## Things TODO

* [] Add Hops lookup and override of AA

# Notes

Currently I didn't add my notes, I use [Denote](https://github.com/protesilaos/denote) and still have to work out if I will use them just for this or for other stuff AND how to use silos in a nice way.
