;;; package --- Homebrew utilities that I use in my org tables
;;; Commentary:

;;; Code:
(defun l2gal (litres)
  "Convert from LITRES to gallons."
  (* litres 0.26417))

(defun abv (og fg)
  "Compute ABV from OG and FG."
  (* (- og fg) 135.25)
  )

(defun expected-gravity (total-ppt efficiency volume)
  "Compute expected FG given an EFFICIENCY from a TOTAL-PPT ideal value."
  (+ 1 (/ (* total-ppt (/ efficiency 100.0)) (* volume 1000))))

(defun efficiency (og total-ppt volume)
  "Compute efficiency of the mash."
  (* 100 (/ (* (- og 1) volume 1000) total-ppt))
  )

(defun attenuation (og fg)
  "Compute apparent attenuation"
  (* 100 (/ (- og fg) (- og 1))))

(defun fg (og attenuation)
  "Compute expected FG with theoretic attenuation"
  (+ 1(* (- og 1) (- 1 (/ attenuation 100.0))))
  )

(defun grain-gravity (kg ppt)
  "Compute grain gravity"
  (* kg ppt 2.20462))

(setq ppts '(("Wheat" . 39)
             ("Pilsner" . 37)
             ("Flaked Wheat" . 34)
             ("Flaked Oats" . 33)
             ("Table Sugar" . 46)
             ("Vienna" . 37)
             )
      )

(defun alist-keys (alist)
  (mapcar 'car alist))

(defun assoc-completion-at-point (keywords-dict)
  "This is the function to be used for the hook `completion-at-point-functions'. KEYWORDS-DICT is an assoc"
  (interactive)
  (let* (
         (bds (bounds-of-thing-at-point 'symbol))
         (start (car bds))
         (end (cdr bds)))
    (list start end (alist-keys keywords-dict) . nil )))
(defun grain-completion-at-point ()
  (assoc-completion-at-point ppts))

(defun get-ppts (name)
  "Translates the grain to a ppt gravity."
  (or (cdr (assoc name ppts)) 36))


;;  To calculate IBUs, the formula is simple:

;; IBUs = decimal alpha acid utilization * mg/l of added alpha acids

(defun compute-ibus (aa grams volume gravity time)
  "Compute IBUs for a given hop addition of AA rating at GRAMS on a VOLUME for the GRAVITY and TIME specifics."
 (/ (* (decimal-alpha-acid-utilization time gravity) grams (/ aa 100) 1000) (* volume (c-gravity gravity)))
 )

(defun compute-grams-per-ibus (aa volume ibu gravity time)
  (/ (* volume (c-gravity gravity) ibu) (* (/ aa 100) 1000 (decimal-alpha-acid-utilization time gravity))))

(defun c-gravity (gravity)
  (if (> gravity 1.050)
      (+ 1 (/ (- gravity 1.050) 2))
    (* 1.0 1.0)))

;; To calculate the concentration of alpha acids you add to the wort:

;; mg/l of added alpha acids = decimal AA rating * grams hops * 1000
;;                             -------------------------------------
;;                               volume of finished beer in liters

;; or for those of you not using metric units:

;; mg/l of added alpha acids = decimal AA rating * ozs hops * 7490
;;                             -------------------------------------
;;                              volume of finished beer in gallons 

;; You can look up the decimal alpha acid utilization in the utilization table below or calculate it directly using the Bigness factor and the Boil Time factor.

;; decimal alpha acid utilization = Bigness factor * Boil Time factor
(defun decimal-alpha-acid-utilization (time gravity)
  "Compute alpha acid utilization given a TIME in minutes and a GRAVITY."
  (* (bigness-factor gravity) (boil-time-factor time)))

;; The Bigness factor accounts for reduced utilization due to higher wort gravities. Use an average gravity value for the entire boil to account for changes in the wort volume.

;; Bigness factor = 1.65 * 0.000125^(wort gravity - 1)

(defun bigness-factor (gravity)
  "Compute alpha acid utilization given a GRAVITY."
  (* 1.65 (expt 0.000125 (- gravity 1))))

;; The Boil Time factor accounts for the change in utilization due to boil time:

;; Boil Time factor = 1 - e^(-0.04 * time in mins)
;;                    --------------------------
;;                              4.15


(defun boil-time-factor (time)
  "Function to compute a part of the acid utilization over TIME in minutes."
  (/ (- 1 (exp (* -0.04 time))) 4.15))

(provide 'homebrewing)
;;; homebrewing.el ends here
;; 
